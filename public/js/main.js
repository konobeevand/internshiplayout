let btnOpen = document.querySelector('#btnOpen')
let btnClose = document.querySelector('#btnClose')
let nav = document.querySelector('#nav')
let body =  document.body
function open (){
    body.style.overflow = 'hidden';
    nav.style.transform = 'translateX(100vw)';
}
function close (){
    body.style.overflow = 'auto';
    nav.style.transform = 'translateX(-100vw)';
}
btnOpen.addEventListener('click', open )
btnClose.addEventListener('click', close )
